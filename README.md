# CAFA 5 Protein Function Prediction
## Description
There is a [kaggle challenge](https://www.kaggle.com/competitions/cafa-5-protein-function-prediction), named exactly same with this this repository. The task is about predicting [the gene ontology](https://geneontology.github.io/) terms of some proteins, using their amino acid sequences. I was able to start it 2 months after it had started, but the fun part never goes away: challenge! 

## Installation
The python requirements together with docker files are given in the repository, so the following steps should bring your development environment up:

#> `docker build -t  cafa5 .`

#> `docker compose up -d`


## Project status
Currently the project is at its initial stages.

I will be updating here, as I progress. 
