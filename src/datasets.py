import os
import numpy as np
import pandas as pd
import torch
from torch.utils.data import Dataset
from typing import Optional, Tuple, Dict
import timeit

class T5EmbeddingsDataset(Dataset):
	"""Torch Dataset class for t5 embeddings"""
	def __init__(
			self, type: str, base_data_dir: str, 
			term_index_map: Optional[Dict[str, int]]=None
		) -> None:
		"""Initializes the class by reading the data.
		For the test data, there will be no label read.

		Parameters:
			type: Either "train" or "test". For test data, only the 
				embeddings will be returned/stored but no labels.
		"""
		self.type = type
		
		# Read the t5 embeddings to create a dataframe
		t5_embeds = np.load(
	    os.path.join(
		        base_data_dir, 
		        f"""t5embeds/{type}_embeds.npy"""
		    )
		)
		t5_embeds = t5_embeds.astype(np.float32)
		t5_ids = np.load(
		    os.path.join(
		        base_data_dir, 
		        f"""t5embeds/{type}_ids.npy"""
		    )
		)
		self.t5_embeds = pd.DataFrame(
			data=t5_embeds, index=t5_ids
		).add_prefix("Embed_")

		del t5_embeds, t5_ids

		if type == "train":
			self.train_terms = pd.read_csv(
			    os.path.join(
			        base_data_dir, 
			        "cafa-5-protein-function-prediction/Train/train_terms.tsv"
			    ), 
			    sep="\t"
			)
			self._create_multiclass_dict()
			self._create_multiclass_labels()
		elif type == "test":
			if term_index_map == None:
				raise ValueError("term_index_map should be a dict!")
			
			self.term_index_map = term_index_map
		else:
			raise ValueError(
				'The input ´type´ should be either "train" or "test".' +
				' But it is given as "'+ type + '" !'
			)

	def __len__(self) -> int:
		"""Return the length of dataset"""
		return self.t5_embeds.shape[0]
	
	@property
	def feats_shape(self) -> Tuple[int,int]:
		"""Shape of the input/features
		
		Just like in the pandas.DataFrame, returns the shape variable of the 
		the features with a tuple indicating the number of rows and columns,
		respectively.
		"""
		return self.t5_embeds.shape
	
	@property
	def n_terms(self) -> int:
		"""Number of GO terms in the dataset"""
		return len(self.term_index_map)

	def __getitem__(self, idx: int) -> Tuple[np.array, Optional[np.array]]: 
		"""Embedding and label of a protein
		
		Parameters:
			idx: Index of a protein in the dataset

		Returns:
			embedding, label: Embedding and the label of the protein
				with the given index: idx.
		"""

		if self.type == "train":
			return (
				self.t5_embeds.iloc[idx, :].to_numpy()
				, self.protein_labels[idx]
			)
		else:
			return self.t5_embeds.iloc[idx, :].to_numpy(), None

	def _create_multiclass_dict(self) -> None:
		"""Create a dict to be used in multiclass label creation
		
		The CAFA 5 protein function prediction challenge is a multitarget
		labeling one, meaning for one entry (protein) there are multiple
		terms/labels to be predicted. This will be converted to a multiclass
		problem by assigning 1 to the active terms if the protein has a
		label for that term. In the end, the dict here will have keys
		as the terms, and the values will be the index of the term in
		the complete terms list.
		The train_terms.tsv file will be used to create this mapping.
		
		Returns:
			term_index_mapping: GO term to its unique index mapping.
		"""

		self.term_index_map = {
			term:i for i, term in enumerate(
				self.train_terms.term.unique()
				)
		}


	def _create_multiclass_labels(self) -> None:
		"""Create multiclass labels for CAFA-5 for training

		The label for each proteins is given as a GO term. This function
		takes the multiple rows in this label data for each protein,
		then converts this to multiclass label. This is achieved using
		the term_index_map, where each term has an index, and by switching its value 
		to 1 for each protein, if the protein contains this term in the 
		ontology. In the end, one-to-one mapping of protein -> label is created.
		"""
		
		# Group the proteins for faster fetch of terms of a protein
		protein_groups = self.train_terms.groupby("EntryID")
		
		# Create all 0s for protein labels.
		self.protein_labels = np.zeros(
			(len(protein_groups), len(self.term_index_map))
		)
		def set_protein_label(terms):
			protein_ind = self.t5_embeds.index.get_loc(terms.iloc[0].EntryID)

			for term in terms.term:
				self.protein_labels[protein_ind, self.term_index_map[term]] = 1

		protein_groups.apply(lambda terms: set_protein_label(terms))

		del protein_groups
		
