import lightning.pytorch as pl
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader, Subset
from torch import optim
from sklearn.model_selection import train_test_split
from torchmetrics.classification import MultilabelF1Score


class BaseCAFAModel(pl.LightningModule):
    """Base lightning model for CAFA 5"""
    def __init__(
            self, batch_size: int, n_terms: int
            , lr: float, train_dataset: Dataset
            ) -> None:
        """Initialize the model functions

        Parameters:
            n_terms: Number of GO terms to predict
            train_dataset: Training dataset
        """
        super().__init__()
        self.batch_size = batch_size
        self.n_terms = n_terms
        self.lr = lr
        self.train_dataset = train_dataset

        self.f1score = MultilabelF1Score(num_labels=self.n_terms, threshold=0.5)
    
    def setup(self, stage: str) -> None:
        """Setup the data"""
        train_ids, val_ids = train_test_split(
            range(len(self.train_dataset)), shuffle=True, test_size=0.15
        )
        self.train_subset = Subset(self.train_dataset, train_ids)
        self.val_subset = Subset(self.train_dataset, val_ids)

    def training_step(self, batch: torch.Tensor, batch_idx: int) -> torch.Tensor:
        """Training of the model"""
        x, y = batch
        y_hat = self(x)
        loss = F.binary_cross_entropy_with_logits(y_hat, y)
        self.log("train_loss", loss)
        return loss
    
    def validation_step(self, batch: torch.Tensor, batch_idx: int) -> torch.Tensor:
        x, y = batch

        y_hat = self(x)
        loss = F.binary_cross_entropy_with_logits(y_hat, y)
        
        self.f1score(y_hat, y)

        self.log("val_loss", loss)
        self.log("val_f1", self.f1score)

    def predict_step(
            self, batch: torch.Tensor, 
            batch_idx: int
        ) -> torch.Tensor:
        """Predict with the model
        
        After the model output (forward method), sigmoid is applied to 
        bring the output between 0 and 1. Then the threshold of 0.5 is
        applied to return class predictions.

        Returns:
            class_preds: Class predictions for the input 
        """
        x = batch
        y_hat = F.sigmoid(self(x))
        return y_hat
    
    def configure_optimizers(self):
        """Optimizer of the model"""
        optimizer = optim.Adam(self.parameters(), self.lr)
        return optimizer
    
    def train_dataloader(self) -> DataLoader:
        return DataLoader(
            self.train_subset, batch_size=self.batch_size
            , num_workers=3
        )
    
    def val_dataloader(self) -> DataLoader:
        return DataLoader(
            self.val_subset, batch_size=self.batch_size, num_workers=3
        )

class ModelFactory(BaseCAFAModel):
    def __init__(self, train_dataset: Dataset, config: dict) -> None:
        super().__init__(
            batch_size=config["batch_size"], n_terms=config["n_terms"]
            ,lr = config["lr"], train_dataset=train_dataset
        )
        self.model_hps = config["model"]
        self.seq = nn.Sequential()
        # in channels for the initial layer, which is the length of
        # 1 protein embedding
        tmp_in_channels = train_dataset.feats_shape[1]
        for tmp_out_ind in range(self.model_hps["num_linear_layers"]):
            self.seq.append(
                nn.Linear(tmp_in_channels, self.model_hps["linear_out_size"])
            )
            self.seq.append(nn.ReLU())
            tmp_in_channels = self.model_hps["linear_out_size"]
        
        # Append last linear layer to predict GO terms
        self.seq.append(nn.Linear(self.model_hps["linear_out_size"], self.n_terms))

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Forward run of the multilabel classification model
        
        
        Note: The sigmoid function in the end is not called here to 
        be able to use logits in the training step. The predict step
        should contain the sigmoid and (if needed) do thresholding.

        Returns:
            logits of the model output
        """
        return self.seq(x)