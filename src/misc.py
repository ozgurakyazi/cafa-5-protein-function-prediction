from lightning.pytorch.callbacks import Callback
from typing import List

class ValMetricsCallback(Callback):
    """PyTorch Lightning metric callback
    This callback is used to save all the validation metrics in 
    a lightning model.
    """
    def __init__(self, metric_names: List[str]):
        super().__init__()
        self.metrics = {metric:[] for metric in metric_names}

    def on_validation_epoch_end(self, trainer, pl_module):
        trainer.callback_metrics
        for metric in self.metrics.keys():
            self.metrics[metric].append(
                trainer.callback_metrics[metric].item()
            )