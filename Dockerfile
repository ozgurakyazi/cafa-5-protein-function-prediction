FROM nvidia/cuda:11.8.0-cudnn8-devel-ubuntu18.04
LABEL authors="Ozgur Akyazi"

ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt -y update && apt -y upgrade && apt-get install -y awscli build-essential \
  python3.8 python3-pip python3.8-dev wget curl gcc g++ make libcairo2-dev pkg-config libssl-dev \
  libgirepository1.0-dev gir1.2-gtk-3.0 default-jdk

RUN wget https://github.com/Kitware/CMake/releases/download/v3.16.5/cmake-3.16.5.tar.gz &&\
  tar -zxvf cmake-3.16.5.tar.gz &&\
  cd cmake-3.16.5 &&\
  ./bootstrap  &&\
  make -j 4 &&\
  make install
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3.8 10
RUN python3.8 -m pip -q install pip --upgrade
RUN python3.8 -m pip install --ignore-installed PyYAML

WORKDIR /srv

RUN python3.8 -m pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118
ADD ./requirements.txt /srv/requirements.txt
RUN python3.8 -m pip install -r requirements.txt


WORKDIR /src
ENTRYPOINT jupyter notebook --ip 0.0.0.0 --allow-root --NotebookApp.token='' --NotebookApp.password=''
